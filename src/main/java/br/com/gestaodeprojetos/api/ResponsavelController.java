package br.com.gestaodeprojetos.api;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestaodeprojetos.domain.model.ResponseApi;
import br.com.gestaodeprojetos.domain.model.dto.ResponsavelDto;
import br.com.gestaodeprojetos.domain.service.ResponsavelService;

@RestController
@RequestMapping("/responsaveis")
public class ResponsavelController {
	
	@Autowired
	private ResponsavelService service;
	
	
	
	@CrossOrigin(origins = "*")
	@PostMapping
	public ResponseEntity<ResponseApi<ResponsavelDto>> salvaResponsavel(@Valid @RequestBody ResponsavelDto dto, BindingResult result) {
		
		ResponseApi<ResponsavelDto> response = new ResponseApi<ResponsavelDto>();
		
		if(result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()) );
			return ResponseEntity.badRequest().body(response);
		}
		
		if(service.responsavelJaExiste(dto)) {
			response.getErrors().add("Responsavel já existe!");
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setData(service.create(dto));
		
		
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping
	public ResponseEntity<ResponseApi<ResponsavelDto>> atualizaResponsavel(@Valid @RequestBody ResponsavelDto dto, BindingResult result) {
		
		ResponseApi<ResponsavelDto> response = new ResponseApi<ResponsavelDto>();
		
		if(result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()) );
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setData(service.atualiza(dto));
		
		return ResponseEntity.ok(response);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(value = "/{id}")
	public void excluiResponsavel(@PathVariable("id") Long id) {
		service.exclui(id);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping
	public ResponseEntity<ResponseApi<ResponsavelDto>> consulta(@RequestParam String nome, @RequestParam int page, @RequestParam int rows) {
		
		Pageable pageable = new PageRequest(page, rows);
		ResponseApi<ResponsavelDto> response = new ResponseApi<ResponsavelDto>();
		response.setDataLists(service.findAllPaginationContaining(nome, pageable));
		return ResponseEntity.ok(response);
	}


}

package br.com.gestaodeprojetos.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/processos")
public class ProcessoController {
	
	@GetMapping
	public String getProcesso() {
		return "Servidor rodando";
	}


}

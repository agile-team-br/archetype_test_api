package br.com.gestaodeprojetos.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.gestaodeprojetos.domain.model.Responsavel;

@Repository
public interface ResponsavelRepository extends PagingAndSortingRepository<Responsavel, Long>{

	Optional<Responsavel> findByCpf(String cpf);
	
	List<Responsavel> findAllByNomeContaining(String nome, Pageable pageable);
	
}

package br.com.gestaodeprojetos.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gestaodeprojetos.domain.model.Processo;

public interface ProcessoRepository extends JpaRepository<Processo, Long>{

}

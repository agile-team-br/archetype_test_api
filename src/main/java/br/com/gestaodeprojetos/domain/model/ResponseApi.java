package br.com.gestaodeprojetos.domain.model;

import java.util.ArrayList;
import java.util.List;

public class ResponseApi<T> {

	private T data;
	private Object dataLists;
	private List<String> errors;

	public ResponseApi() {
	}
	
	

	public Object getDataLists() {
		return dataLists;
	}



	public void setDataLists(Object dataLists) {
		this.dataLists = dataLists;
	}



	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<String> getErrors() {
		if (this.errors == null) {
			this.errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

}

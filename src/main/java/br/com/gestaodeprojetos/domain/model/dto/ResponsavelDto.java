package br.com.gestaodeprojetos.domain.model.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

public class ResponsavelDto {
	
	
	public ResponsavelDto () {}
	public ResponsavelDto(String nome, String cpf, String email) {
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
	}
	
	private Long id;

	@NotEmpty
	@Length(min=5, max=150, message = "Nome deve possuir no máximo 150 caracteres!")
	private String nome;

	@NotNull(message = "Email não pode ser vazio")
	@Email(message = "Email inválido")
	@Length(min=5, max=400, message = "Email não pode possuir mais de 400 caracteres!")
	private String email;

	@NotEmpty(message = "CPF não pode ser vazio")
	@CPF(message = "CPF inválido!")
	private String cpf;

	private String foto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	

}

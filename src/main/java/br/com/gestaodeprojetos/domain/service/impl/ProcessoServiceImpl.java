package br.com.gestaodeprojetos.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gestaodeprojetos.domain.model.Processo;
import br.com.gestaodeprojetos.domain.repository.ProcessoRepository;
import br.com.gestaodeprojetos.domain.service.ProcessoService;

@Service
public class ProcessoServiceImpl implements ProcessoService{
	
	@Autowired
	private ProcessoRepository processoRepository;

	@Override
	public Processo create(Processo processo) {
		return processoRepository.save(processo);
	}

	@Override
	public void delete(Processo note) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Processo> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

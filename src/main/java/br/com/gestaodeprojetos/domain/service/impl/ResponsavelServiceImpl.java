package br.com.gestaodeprojetos.domain.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestaodeprojetos.api.ResponsavelController;
import br.com.gestaodeprojetos.domain.converter.ResponsavelConverter;
import br.com.gestaodeprojetos.domain.model.Responsavel;
import br.com.gestaodeprojetos.domain.model.dto.ResponsavelDto;
import br.com.gestaodeprojetos.domain.repository.ResponsavelRepository;
import br.com.gestaodeprojetos.domain.service.ResponsavelService;

@Service
public class ResponsavelServiceImpl implements ResponsavelService{
	

	@Autowired
	private ResponsavelConverter converter;
	
	@Autowired
	private ResponsavelRepository repository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponsavelController.class);
	
	
	@Override
	public ResponsavelDto create(ResponsavelDto dto) {
	
		Responsavel responsavelCriado = new Responsavel();
		
		responsavelCriado = repository.save(converter.toModel(dto));
		LOGGER.info("responsavel criado");
		
		return converter.toDto(responsavelCriado);
	}

	@Override
	public boolean responsavelJaExiste(ResponsavelDto dto) {
		return repository.findByCpf(dto.getCpf()).isPresent();
	}

	@Override
	public List<ResponsavelDto> findAllPaginationContaining(String nome, Pageable pageable) {
		return converter.toListDto(repository.findAllByNomeContaining(nome, pageable));
	}

	@Override
	public ResponsavelDto atualiza(ResponsavelDto dto) {
		
		Responsavel responsavelAlterado = new Responsavel();
		
		responsavelAlterado = repository.save(converter.toModel(dto));
		
		return converter.toDto(responsavelAlterado);
	}

	@Override
	public void exclui(Long id) {
		repository.delete(id);
	}

}

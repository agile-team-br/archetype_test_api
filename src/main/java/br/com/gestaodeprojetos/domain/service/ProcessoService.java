package br.com.gestaodeprojetos.domain.service;

import java.util.List;

import br.com.gestaodeprojetos.domain.model.Processo;

public interface ProcessoService {
	
	Processo create(Processo note);
	void delete(Processo note);
	List<Processo> findAll();

}

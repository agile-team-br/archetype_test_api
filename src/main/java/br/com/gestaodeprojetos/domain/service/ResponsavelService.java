package br.com.gestaodeprojetos.domain.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import br.com.gestaodeprojetos.domain.model.dto.ResponsavelDto;

public interface ResponsavelService {
	
	ResponsavelDto create(ResponsavelDto responsavel);

	boolean responsavelJaExiste(ResponsavelDto dto);

	List<ResponsavelDto> findAllPaginationContaining(String nome, Pageable pageable);

	ResponsavelDto  atualiza(ResponsavelDto dto);

	void exclui(Long id);
}

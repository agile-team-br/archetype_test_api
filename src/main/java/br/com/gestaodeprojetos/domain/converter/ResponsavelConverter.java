package br.com.gestaodeprojetos.domain.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.gestaodeprojetos.domain.model.Responsavel;
import br.com.gestaodeprojetos.domain.model.dto.ResponsavelDto;

@Service
public class ResponsavelConverter {
	
	public ResponsavelDto toDto(Responsavel model) {
		
		ResponsavelDto dto = new ResponsavelDto();
		
		dto.setId(model.getId());
		dto.setCpf(model.getCpf());
		dto.setNome(model.getNome());
		dto.setEmail(model.getEmail());
		dto.setFoto(model.getFoto());
		return dto;
	}

	public Responsavel toModel(ResponsavelDto dto) {
		
		Responsavel model = new Responsavel();
		
		model.setId(dto.getId());
		model.setCpf(dto.getCpf());
		model.setNome(dto.getNome());
		model.setEmail(dto.getEmail());
		model.setFoto(dto.getFoto());
		
		return model;
	}

	public List<ResponsavelDto> toListDto(List<Responsavel> listModel) {
		
		List<ResponsavelDto> listDto = new ArrayList<ResponsavelDto>();
		
		listModel.forEach(m -> {
			listDto.add(toDto(m));
		});
		
		return listDto;
	} 
}

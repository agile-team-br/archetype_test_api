package br.com.gestaodeprojetos.integrationTests.domain.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gestaodeprojetos.domain.model.dto.ResponsavelDto;
import br.com.gestaodeprojetos.domain.service.ResponsavelService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ResponsavelServiceTest {
	
	@Autowired
	private ResponsavelService service;
	
	private ResponsavelDto dto;
	
	@Before
	public void setup() {
		dto = new ResponsavelDto("Ricardo", "05152634783", "ricanalista@gmail.com");
	}
	
	
	@Test
	public void deveCriarUmNovoResponsavel() {
		
		ResponsavelDto response = service.create(dto);
		
		assertEquals(response.getCpf(), "05152634783");
		
	}
	
	@Test
	public void deveValidarSeJaExisteResponsavelPeloCpf() {
		assertTrue(service.responsavelJaExiste(dto));
	}
	
	@Test
	public void deveBuscarPaginadoPorNome() {
		Pageable pageable = new PageRequest(0, 10);
		List<ResponsavelDto> listResponsavelDto = new ArrayList<ResponsavelDto>();
		listResponsavelDto = service.findAllPaginationContaining("Ricardo", pageable);
		assertThat(listResponsavelDto.size(), is(1));
	}
	
	

}

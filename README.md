# API Gestão de Processos Judiciais

Api REST de integração de processos judiciais.

_Prerequisitos:_
* docker version 1.10.3
* JDK8
* Maven

## Instalação

1. Seguindo a ordem de instalação você precisará do docker instalado em sua máquina para gerar a imagem da aplicação com o seguinte comando na raiz do projeto:

   `mvn clean package docker:build -Dmaven.test.skip=true`
   
2. Uma vez a imagem criada você poderá rodar a aplicação com o seguinte comando:

   `docker run -p 8080:8080 api_softplan_web`

3. Uma vez a imagem levantada você vai poder acessar e testar pelas urls:

   Entrypoint Swagger >> `http://localhost:8080/api-processos/swagger-ui.html#`  
   

## Features

- `Liquibase` para gerenciar banco de dados e migrations.
- `Springboot` 

